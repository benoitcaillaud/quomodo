#!/usr/bin/python
# client Quomodo, envoyant les commandes au démon quomodod
# (c) Benoît Caillaud, 2016,2023

import sys
import socket

# command length

command_length=80

# Version

version='0.2a'

# Socket name

work_dir = '/home/quomodo/run/'
sock_name = work_dir + 'quomodod.socket'

# Usage...

def usage():
  print('{} v.{}, (c) Benoit Caillaud <benoit@caillaud.eu>, 2016, 2023'.format(sys.argv[0],version))
  print('Usage: {} <device> <state>'.format(sys.argv[0]))
  
# Programme principal

if __name__ == '__main__':

  # fail if too few arguments
  
  if len(sys.argv) <= 1:
    print('{}: too few arguments'.format(sys.argv[0]))
    usage()
    sys.exit(-1)

  # Concatenate command arguments
    
  s = sys.argv[1]
  for i in range(2,len(sys.argv)):
    s += ' ' + sys.argv[i]

  # pad with white spaces

  while len(s) < command_length:
    s += ' '

  if len(s) > command_length:
    print('{}: command line too long'.format(sys.argv[0]))
    usage()
    sys.exit(-1)
    
  # open socket and connect to server

  try:
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
  except:
    print('{}: failed to create socket'.format(sys.argv[0],sock_name))
    usage()
    sys.exit(-1)

  try:
    sock.connect(sock_name)

    # send command and close socket

    sock.send(s)
    sock.close()

  except:
    print('{}: failed to connect or to send data to socket {}'.format(sys.argv[0],sock_name))
    sock.close()
    usage()
    sys.exit(-1)

  # Done. We can exit.
    
  sys.exit(0)
