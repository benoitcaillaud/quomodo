#!/usr/bin/python
# démon Quomodo, recevant les commandes et commandant le périphérique 1-wire 8 relais
# (c) Benoît Caillaud, 2016,2023

import threading
import time
from datetime import date
import socket
import struct
import sys
import signal
import os
import stat
import pwd

# Fonction d'ecriture sur le PIO DS2408 connecte sur le bus 1-wire

def w1_output(id,value):
  f = open('/sys/bus/w1/devices/' + id + '/output','wb',0)
  try:
    s = struct.pack('=B',255-value)
    f.write(s)
    f.close()
    return 0
  except:
    f.close()
    return 0

# Interface et parametres thread ecriture sur le PIO

periode = 1.0
verrou = threading.Lock()
registre = 255
port = ""
terminate = False
command_length=80

# Thread d'ecriture periodique sur le PIO DS2408

class ThreadEcriture(threading.Thread):
    def run(self):
      global registre
      global verrou
      global port
      global periode
      global terminate
      while not terminate:
        verrou.acquire()
        valeur = registre
        verrou.release()
        w1_output(port,valeur)
        time.sleep(periode)

# Masques et codes relaise

FP_M   = 0b11
FP_POS = 0b01
FP_NEG = 0b10
FP_1   = 0
FP_2   = 2
FP_3   = 4
RLY_M  = 0b1
RLY_1  = 6
RLY_2  = 7

# Commandes et types appareils

filPilote = {'normal' : 0, 'economique' : FP_POS+FP_NEG, 'hors-gel' : FP_NEG, 'arret' : FP_POS,
             'on' : 0, 'eco' : FP_POS+FP_NEG, 'hg' : FP_NEG, 'off' : FP_POS}

revFilPilote = { 0b00 : 'normal', 0b11 : 'economique', 0b10 : 'hors-gel', 0b01 : 'arret' }

contacteur = {'marche' : 0, 'arret' : 1,
              'on' : 0, 'off' : 1}

revContacteur = { 0b0 : 'marche', 0b1 : 'arret' }

commande = {
  'chambres' : { 'commande' : filPilote, 'offset' : FP_1, 'masque' : FP_M },
  'bedrooms' : { 'commande' : filPilote, 'offset' : FP_1, 'masque' : FP_M },
  'ch' : { 'commande' : filPilote, 'offset' : FP_1, 'masque' : FP_M },
  'salles-de-bains' : { 'commande' : filPilote, 'offset' : FP_2, 'masque' : FP_M },
  'sdb' : { 'commande' : filPilote, 'offset' : FP_2, 'masque' : FP_M },
  'rez-de-chaussee' : { 'commande' : filPilote, 'offset' : FP_3, 'masque' : FP_M },
  'rdc' : { 'commande' : filPilote, 'offset' : FP_3, 'masque' : FP_M },
  'eau-chaude-sanitaire' : { 'commande' : contacteur, 'offset' : RLY_1, 'masque' : RLY_M },
  'ecs' : { 'commande' : contacteur, 'offset' : RLY_1, 'masque' : RLY_M },
  'chauffage' : { 'commande' : contacteur, 'offset' : RLY_2, 'masque' : RLY_M },
  'cfg' : { 'commande' : contacteur, 'offset' : RLY_2, 'masque' : RLY_M }}

# Evaluation commandes

def evaluer(etat,objet,cmd):
  try:
    s = commande[objet]
    c = s['commande']
    o = s['offset']
    m = s['masque']
    try:
      n = c[cmd]
      return (n << o) | ((~(m << o)) & etat)
    except:
      print('Erreur : commande {} inconnue'.format(cmd))
      return etat
  except:
    print('Erreur : objet {} inconnu'.format(objet))
    return etat

def evaluer_ligne(etat,s):
  try:
    p = s.split(' ')
    obj=p[0]
    cmd=p[1]
    return evaluer(etat,obj,cmd)
  except:
    print('Erreur de syntaxe dans la ligne : {}'.format(s))
    return etat

# Lecture et ecriture status; status par defaut

def defaut():
    etat = 0
    etat = evaluer_ligne(etat,'chambres normal')
    etat = evaluer_ligne(etat,'rez-de-chaussee normal')
    etat = evaluer_ligne(etat,'salles-de-bains normal')
    etat = evaluer_ligne(etat,'eau-chaude-sanitaire marche')
    etat = evaluer_ligne(etat,'chauffage marche')
    return etat

def lire_etat(fichier):
  try:
    f = open(fichier,'rb',0)
    try:
      etat = 0
      for s in f:
        if len(s) > 0:
          if s[0] != '%':
            etat = evaluer_ligne(etat,(s.split('\n'))[0])
      f.close()
      return etat
    except:
      f.close()
      return defaut()
  except:
    return defaut()

# sauvegarde etat

def formater(division,table,etat,offset,masque):
  # print('(division,etat,offset,masque)=({},{:08b},{},{})'.format(division,etat,offset,masque))
  n = (etat >> offset) & masque
  # print('Bits : {:02b}'.format(n))
  s = table[n]
  return division + ' ' + s + '\n'

def ecrire_etat(fichier,etat):
  try:
    os.umask(0137)
    f = open(fichier,'w',1)
    try:
      d = date.today()
      s = '% Sauvegarde etat le ' + d.isoformat() + '\n'
      s = s + formater('eau-chaude-sanitaire',revContacteur,etat,RLY_1,RLY_M)
      s = s + formater('chauffage',revContacteur,etat,RLY_2,RLY_M)
      s = s + formater('chambres',revFilPilote,etat,FP_1,FP_M)
      s = s + formater('salles-de-bains',revFilPilote,etat,FP_2,FP_M)
      s = s + formater('rez-de-chaussee',revFilPilote,etat,FP_3,FP_M)
      f.write(s)
      f.close()
    except:
      f.close()
      print('Echec ecriture dans fichier {}'.format(fichier))
  except:
    print('Echec ouverture fichier {}'.format(fichier))
          
# Thread interprete

class ThreadInterprete(threading.Thread):
  def __init__(self, session, client):
    self.session = session
    threading.Thread.__init__(self)
    
  def run(self):
    global registre
    global verrou

    # receive command
    
    s = session.recv(command_length)

    # process command
    
    print('Commande recue : {}'.format(s))
    
    if len(s) > 0:
      if s[0] != '%':
        verrou.acquire()
        registre = evaluer_ligne(registre,(s.split('\n'))[0])
        ecrire_etat(status_name,registre)
        print('Nouvel etat = {:08b}'.format(registre))
        verrou.release()

    # Close socket

    session.close()

# SIGTERM handler

def signal_term_handler(signal, frame):
  raise KeyboardInterrupt
    
# Version

version='0.1a'

# Programme principal

if __name__ == '__main__':
  
  # print banner
  
  print('quomodod v.{} starting, (c) Benoit Caillaud <benoit@caillaud.me>, 2016'.format(version))

  # handle SIGTERM

  signal.signal(signal.SIGTERM, signal_term_handler)

  # set constants
  
  port = '29-0000001ad7b6'
  device = '/sys/bus/w1/devices/' + port + '/output'
  device_mod = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IWGRP | stat.S_IROTH | stat.S_IWOTH
  user_name = 'quomodo'

  # retrieve uid, gid and home dir

  try:
    pw = pwd.getpwnam(user_name)
    user_id = pw.pw_uid
    group_id = pw.pw_gid
    work_dir = pw.pw_dir + '/run/'
    status_name = work_dir + 'quomodod.sta'
    sock_name = work_dir + 'quomodod.socket'
    pid_name = work_dir + 'quomodod.pid'
  except:
    print('quomodod: failed to change gid/uid to {}/{}'.format(user_id,group_id))
    work_dir = '/home/quomodo/run/'
    status_name = work_dir + 'quomodod.sta'
    sock_name = work_dir + 'quomodod.socket'
    pid_name = work_dir + 'quomodod.pid'

  # set mods for device output

  try:
    os.chmod(device,device_mod)
  except:
    print('quomodod: failed to chmod device {}'.format(device))

  # change uid and gid and chdir

  try:
    os.setgid(group_id)
    os.setuid(user_id)
    os.chdir(work_dir)
  except:
    print('quomodod: failed to change gid/uid to {}/{}'.format(user_id,group_id))
    
  # fork daemon
  
  daemon_pid = os.fork()

  if daemon_pid != 0:

    # parent of the daemon: write daemon's pid to file
    
    f = open(pid_name,'w',1)
    f.write(str(daemon_pid))
    f.close()
    sys.exit(0)

  else:

    # daemon: initalize writer thread and input socket
    
    registre = lire_etat(status_name)
    ecriture = ThreadEcriture()
    
    try:
      
      # remove socket if already exists

      try:
        os.unlink(sock_name)
      except OSError:
        if os.path.exists(sock_name):
          raise

      # open socket and bind it

      os.umask(0117)
    
      sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
      sock.bind(sock_name)
      sock.listen(1)

      # start thread ecriture
    
      ecriture.start()

      # wait for a connection and spawn a interprete thread
    
      while True:

        session, client = sock.accept()

        interprete = ThreadInterprete(session, client)
        interprete.start()

    except KeyboardInterrupt:
      sock.close()
      verrou.acquire()
      registre = 0
      verrou.release()
      time.sleep(2.5*periode)
      terminate=True
      time.sleep(2.5*periode)
      sys.exit(0)
