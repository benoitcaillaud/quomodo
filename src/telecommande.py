#!/usr/bin/python

import time
import struct
import sys

def w1_output(id,value):
  f = open('/sys/bus/w1/devices/' + id + '/output','wb',0)
  try:
    s = struct.pack('=B',255-value)
    f.write(s)
    f.close()
    return 0
  except:
    f.close()
    return 0

# Masques et codes relais

FP_M   = 0b11
FP_POS = 0b01
FP_NEG = 0b10
FP_1   = 0
FP_2   = 2
FP_3   = 4
RLY_M  = 0b1
RLY_1  = 6
RLY_2  = 7

# Commandes et types appareils

filPilote = {'normal' : 0, 'economique' : FP_POS+FP_NEG, 'hors-gel' : FP_NEG, 'arret' : FP_POS,
             'on' : 0, 'eco' : FP_POS+FP_NEG, 'hg' : FP_NEG, 'off' : FP_POS}

contacteur = {'marche' : 1, 'arret' : 0,
              'on' : 1, 'off' : 0}

commande = {
  'chambres' : { 'commande' : filPilote, 'offset' : FP_1, 'masque' : FP_M },
  'bedrooms' : { 'commande' : filPilote, 'offset' : FP_1, 'masque' : FP_M },
  'ch' : { 'commande' : filPilote, 'offset' : FP_1, 'masque' : FP_M },
  'salles-de-bains' : { 'commande' : filPilote, 'offset' : FP_2, 'masque' : FP_M },
  'sdb' : { 'commande' : filPilote, 'offset' : FP_2, 'masque' : FP_M },
  'rez-de-chaussee' : { 'commande' : filPilote, 'offset' : FP_3, 'masque' : FP_M },
  'rdc' : { 'commande' : filPilote, 'offset' : FP_3, 'masque' : FP_M },
  'eau-chaude-sanitaire' : { 'commande' : contacteur, 'offset' : RLY_1, 'masque' : RLY_M },
  'ecs' : { 'commande' : contacteur, 'offset' : RLY_1, 'masque' : RLY_M },
  'chauffage' : { 'commande' : contacteur, 'offset' : RLY_2, 'masque' : RLY_M },
  'cfg' : { 'commande' : contacteur, 'offset' : RLY_2, 'masque' : RLY_M }}

# Evaluation commandes

def evaluer(etat,objet,cmd):
  try:
    s = commande[objet]
    c = s['commande']
    o = s['offset']
    m = s['masque']
    try:
      n = c[cmd]
      return (n << o) | ((~(m << o)) & etat)
    except:
      print('Erreur : commande {} inconnue'.format(cmd))
      return etat
  except:
    print('Erreur : objet {} inconnu'.format(objet))
    return etat

def evaluer_ligne(etat,s):
  try:
    p = s.split(' ')
    obj=p[0]
    cmd=p[1]
    return evaluer(etat,obj,cmd)
  except:
    print('Erreur de syntaxe dans la ligne : {}'.format(s))
    return etat

# Lecture et ecriture status

# def lire_etat(fichier):
#   try:
#     f = open(fichier,'rb',0)
#     try:
#       ...
#     except:
#   except:
#     etat=0
#     etat=evaluer_ligne(etat,'chambres arret')
#     etat=evaluer_ligne(etat,'rez-de-chaussee arret')
#     etat=evaluer_ligne(etat,'salles-de-bains arret')
#     etat=evaluer_ligne(etat,'eau-chaude-sanitaire arret')
#     etat=evaluer_ligne(etat,'chauffage arret')
#     return etat

# Version

version='0.1a'

# Programme principal

if __name__ == '__main__':
  id = '29-0000001ad7b6'
  status = 'telecommande.sta'
  print('telecommande v.{}, (c) Bneoit Caillaud <benoit@caillaud.me>, 2016'.format(version))
  etat=0
  etat=evaluer_ligne(etat,'chambres arret')
  etat=evaluer_ligne(etat,'rez-de-chaussee arret')
  etat=evaluer_ligne(etat,'salles-de-bains arret')
  etat=evaluer_ligne(etat,'eau-chaude-sanitaire arret')
  etat=evaluer_ligne(etat,'chauffage arret')
  try:
    print('Etat = {:08b}'.format(etat))
    w1_output(id,etat)
    for s in sys.stdin:
      etat = evaluer_ligne(etat,(s.split('\n'))[0])
      print('Etat = {:08b}'.format(etat))
      w1_output(id,etat)
  except KeyboardInterrupt:
    w1_output(id,0)
    sys.exit(0)
