# Quomodo

A software for controlling electric heaters using the "Fil-pilote'
remote control feature of electric heaters in France. The software is
designed to run on a Raspberry 3B SBC, interfaced to a One-Wire 8 relays
device.

The following authors have contributed to the software:

    Benoît Caillaud <benoit@caillaud.eu>, 2016, 2023

It is distributed as an open-source software under the GPL compatible
CeCILL license:

   http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
